---
layout: markdown_page
title: "What is a continuous integration server?"
description: "Learn more about the role of a continuous integration server in software development."
---

Continuous Integration (CI) is a staple of modern software development. While CI doesn’t technically require specific tools, most teams use a continuous integration server to help them streamline processes.

There are two approaches to CI: Manual builds and builds facilitated by a CI server. Learn the difference between manual and CI server builds and how a CI server works.


## What does a CI server do?

A continuous integration server (sometimes known as a build server) essentially manages the shared repository and acts as a referee for the code coming in. When developers commit to the repository, the CI server [initiates a build](https://www.martinfowler.com/articles/continuousIntegration.html){:target="_blank"} and documents the results of the build. Typically, the developer that committed the code change to the repository will receive an email notification with the results. 

The majority of teams building software today are practicing continuous integration. Of those teams practicing continuous integration, most rely on a CI server to automate builds. 


## CI server vs. manual builds

For teams that do not use a CI server, they’re still able to achieve continuous integration through periodic builds they manage internally. Teams can use scripts they build themselves or manually trigger builds. Continuous integration [isn’t a tool](http://www.jamesshore.com/v2/blog/2005/continuous-integration-is-an-attitude){:target="_blank"} in iself, it’s a larger framework with a set of practices aided by certain tools. 

For teams that want to practice continuous integration, there are some [helpful tools](/topics/ci-cd/implement-continuous-integration/#essential-continuous-integration-tools){:target="_blank"} that can make continuous integration easier. For teams that bypass using a CI server, it’s about having more control over the source code, testing, and commit processes.


## Advantages of a CI server

Teams find CI servers useful in software development because it can offer certain process advantages:


### Automated tests

When code is committed using a CI Server, errors in the code are detected automatically. The server tests the code and provides feedback to the committer quickly, without the committer having to initiate a manual build.


### Better collaboration

Instead of relying on developers to keep the shared repository up to date, the CI server manages all code coming in and maintains the integrity of the source code through automatic builds and tests. This allows developers to focus on their own projects instead of worrying about other projects breaking their tests. Teams can collaborate without worrying about code.


### Streamlined workflow 

Without CI servers, developers are working on different layers of the application with code saved on local machines. While teams that do manual or self-hosted testing can get around these potential issues, a CI server takes this extra layer of coordination out of the workflow. 


## Examples of CI servers

The majority of tools we consider continuous integration tools are, in fact, CI servers. Some include additional functionality, such as source code management, [continuous delivery](/stages-devops-lifecycle/continuous-delivery/){:target="_blank"}, and testing:

*   **[GitLab CI](/stages-devops-lifecycle/continuous-integration/){:target="_blank"}**
*   **[Jenkins](/devops-tools/jenkins-vs-gitlab/){:target="_blank"}**
*   **[CircleCI](/devops-tools/circle-ci-vs-gitlab/){:target="_blank"}**
*   **[Atlassian Bamboo](/devops-tools/bamboo-vs-gitlab/){:target="_blank"}**

## More about continuous integration:

[How to convince leadership to adopt CI/CD](https://page.gitlab.com/2021_eBook_leadershipCICD.html){:target="_blank"}

[Getting [meta] with GitLab CI/CD: Building build images](/blog/2019/08/28/building-build-images/){:target="_blank"}

[A beginner’s guide to continuous integration](/blog/2018/01/22/a-beginners-guide-to-continuous-integration/){:target="_blank"}
